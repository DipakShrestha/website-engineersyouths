import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Example from './components/Example';
import TestComponents from './components/testComponents';
import BlogArticle from './components/blogArticle';
import BlogArticlePost from './components/BlogArticlePost';
import ScrollspyNav from "react-scrollspy-nav";
import Contact from './components/Contact/Contact';
import ServicesComp from './components/ServicesComponent/ServicesComp';
import AboutUs from './components/AboutUs/AboutUs';
import Introduction from './components/Introduction/Introduction';

import './css/index.css';
import { Helmet } from "react-helmet";
import Script from 'react-load-script';
import './css/dropdownhover.css';
import './css/blurbackground.css';
import './css/my_own_card_view.css';
import './css/nav-bar.css';

// import './css/UIkit/uikit-rtl.css';
import './css/UIkit/uikit.css';

import './UIkitJS/uikit-icons.js';
import './UIkitJS/uikit.js';

import './css/blur-hover.css';
import './css/home-design.css';
import './css/carousel-hover.css';

export default class Index extends Component {

    render() {
        return (

            <div style={{background: 'white'}}>
                <NavBarWithContent />

                {/* <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky">
                    <NavBar />
                </div> */}


                {/* <div className="uk-background-fixed uk-background-center-center uk-height-medium uk-width-large" style={{"background-image": "url(http://localhost/laravel-react/tutorial-react/resources/js/imageassest/tihar.jpg)"}}></div> */}
                <div id="home" >
                    {/* <div class="uk-divider-icon"></div> */}
                    <Example />
                </div>
                <hr className="uk-divider-icon"></hr>
                <div id="introduction">
                    <Introduction />
                </div>
                <hr className="uk-divider-icon"></hr>
                <div id="services-above">
                    <ServicesComp />
                </div>
                <hr className="uk-divider-icon"></hr>
                <div id="about-us">
                    <AboutUs />
                </div>
                <hr className="uk-divider-icon"></hr>
                <div id="contact">
                    <Contact />
                </div>
            </div>
        );
    }
}

export class CarouselView extends Component {
    render() {
        return (

            <div className="uk-container" >
                <div id="carouselExampleIndicators" className="carousel slide d-md-block border-black" data-ride="carousel">
                    <ol className="carousel-indicators my_own_card_view">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div className="carousel-inner ">
                        <div className="carousel-item active">

                            <img className="d-block uk-animation-kenburns" src={require("./imageassest/androidAndIosApplication.png")} height="500" alt="First slide" />

                            <div className="carousel-caption d-flex d-md-block">
                                <h5 className="card transparent-background-inner  font-caurosel-caption  ">Remember us for any mobile apps- Android and ios.</h5>

                                {/* <p>...</p> */}
                            </div>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block uk-animation-kenburns" src={require("./imageassest/webapp_website.png")} height="500" alt="Second slide" />
                            <div className="carousel-caption d-flex d-md-block">
                                <h5 className=" card transparent-background-inner font-caurosel-caption">Remember us for all kinds of Website and Webapp development.</h5>

                                {/* <p>...</p> */}
                            </div>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block uk-animation-kenburns" src={require("./imageassest/Embeded.png")} height="500" alt="Third slide" />
                            <div className="carousel-caption d-flex d-md-block">
                                <h5 className=" card transparent-background-inner font-caurosel-caption">We develop power efficient and optimized embedded systems.</h5>

                                {/* <p>...</p> */}
                            </div>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block uk-animation-kenburns" src={require("./imageassest/led_display.jpeg")} height="500" alt="Fourth slide" />
                            <div className="carousel-caption d-flex d-md-block">
                                <h5 className=" card transparent-background-inner font-caurosel-caption">We develop display board as per your need of all sizes and with any customization.</h5>

                                {/* <p>...</p> */}
                            </div>
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>

            </div>
        );
    }
}
class NavBarWithContent extends Component {
    render() {
        return (
            <div className="uk-section-primary uk-preserve-color" id="nav-content">

                <div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; cls-inactive: uk-navbar-transparent; top: 40">
                    <NavBar />
                </div>

                <div className="uk-section" style={{paddingBottom: '0%'}}>
                    <div className="uk-flex uk-flex-center">

                        <div className="uk-container">
                            <div id="nav-section-text" className="card uk-flex uk-flex-center uk-flex-middle">
                                <p className="heading-text" id="top-text" style={{ color: 'white' }} >Welcome to</p>
                                <p className="heading-text" id="middle-text" style={{ color: 'white' }} >Engineers Youths</p>
                                <p className="heading-text" id="bottom-text" style={{ color: 'white' }} >We are dedicated towards the perfection</p>
                            </div>
                        </div>
                    </div>

                    <div className="uk-flex uk-flex-center">

                        <div className="uk-container-expand" >
                            <img style={{objectFit: 'contain'}} src={require("./imageassest/kathmandu.png")}/>
                        </div>
                    </div>
                    {/* <div className="uk-flex uk-flex-center">
            
                        <div class="uk-container">

                            <CarouselView></CarouselView>

                        </div>
                    </div> */}

                </div>

            </div>
        );
    }
}

class NavBar extends Component {
    render() {
        return (
            <nav className="uk-navbar-container uk-margin" uk-navbar="mode: click">
                <div className="uk-navbar-left">

                    <ul className="uk-navbar-nav uk-nav-default" uk-scrollspy-nav="closest: li; scroll: true">
                        <li uk-scrollspy-nav="cls: uk-active" id="image-nav">
                            <a href="#nav-content">
                                <img src={require("./imageassest/iconReady.png")} width="50" height="50" />
                            </a>
                        </li>

                        <li>
                            {/* <a href="#above">Parent</a> */}
                            <a href="#home">Home</a>
                            {/* <div className="uk-navbar-dropdown">
                                        <ul className="uk-nav uk-navbar-dropdown-nav">
                                            <li className="uk-active"><a href="#">Active</a></li>
                                            <li><a href="#">Item</a></li>
                                            <li><a href="#">Item</a></li>
                                        </ul>
                                    </div> */}
                        </li>
                        <li><a href="#introduction">Introduction</a></li>
                        <li><a href="#services-above">Services</a></li>
                        <li><a href="#about-us">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>

                </div>
            </nav>

        );
    }
}


if (document.getElementById('example')) {
    ReactDOM.render(<Index />, document.getElementById('example'));
}
