import React, { Component } from 'react';

export default class Header_Title extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: [],
        }
    }


    render() {
        const title = this.props.title;
        if (title === 'Contact') {
            return (
                <div className="uk-container-extend" >
                    <div className="uk-height-small" style={{ background: "#000000", height: '70px' }}>
                        <h1 className="uk-heading-line uk-text-center" id="heading-title" uk-scrollspy="cls: uk-animation-slide-bottom; delay:50; repeat: true" style={{ color: "#ffffff", paddingTop: '7px' }}><span>{title}</span></h1>
                    </div>
                </div>
            );
        } else if (title === 'Home') {
            return (
                <div className="uk-container-extend" >
                    <div className="uk-height-small" style={{ background: "#000000", height: '70px' }}>
                        <h1 className="uk-heading-line uk-text-center" id="heading-title" uk-scrollspy="cls: uk-animation-slide-bottom; delay:50; repeat: true" style={{ color: "#ffffff", paddingTop: '7px' }}><span>{title}</span></h1>
                    </div>
                </div>
            );
        } else if (title === 'Introduction') {
            return (
                <div className="uk-container-extend" >
                    <div className="uk-height-small" style={{ background: "#000000", height: '70px' }}>
                        <h1 className="uk-heading-line uk-text-center" id="heading-title" uk-scrollspy="cls: uk-animation-slide-bottom; delay:50; repeat: true" style={{ color: "#ffffff", paddingTop: '7px' }}><span>{title}</span></h1>
                    </div>
                </div>
            );
        } else if (title === 'Services') {
            return (
                <div className="uk-container-extend" >
                    <div className="uk-height-small" style={{ background: "#000000", height: '70px' }}>
                        <h1 className="uk-heading-line uk-text-center" id="heading-title" uk-scrollspy="cls: uk-animation-slide-bottom; delay:50; repeat: true" style={{ color: "#ffffff", paddingTop: '7px' }}><span>{title}</span></h1>
                    </div>
                </div>
            );
        }else if(title === 'About'){
            return (
                <div className="uk-container-extend" >
                    <div className="uk-height-small" style={{ background: "#00000f", height: '70px' }}>
                        <h1 className="uk-heading-line uk-text-center" id="heading-title" uk-scrollspy="cls: uk-animation-slide-bottom; delay:50; repeat: true" style={{ color: "#ffffff", paddingTop: '7px' }}><span>{title}</span></h1>
                    </div>
                </div>
            );
        }

    }
}
