import React, { Component } from 'react';
// import '../css/home.css';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';
import BlogArticlePost from './BlogArticlePost';

const carouselItems = [
    {
        src: require('../imageassest/androidAndIosApplication.png'),
        caption: 'Android and IOS Apps',
        description: 'We develop display board as per your need of all sizes and with any customization.'
    },
    {
        src: require('../imageassest/a.png'),
        caption: 'Website and Web Applications',
        description: 'Remember us for all kinds of mobile apps developments.'
    },
    {
        src: require('../imageassest/led_display.jpeg'),
        caption: 'LED Sign Boards',
        description: 'Remember us for all kinds of Website and Webapp development'
    },
    {
        src: require('../imageassest/embeded.gif'),
        caption: 'Customized Embedded Systems',
        description: 'We develop power efficient and optimized embedded systems'
    }
];

export default class Home extends Component {

    render() {
        return (
            <div className="feature-list">
                {/* <Router>
                    <Link to="/test/post-article">
                        <FeatureDisplayUnit
                        image={carouselItems[0].src}
                        caption={carouselItems[0].caption}
                        description={carouselItems[0].description} />
                    </Link>
                    <Route path="/test/post-article" exact component={BlogArticlePost}></Route>
                </Router> */}
                <a href="/test/post-article">
                    <FeatureDisplayUnit
                    image={carouselItems[0].src}
                    caption={carouselItems[0].caption}
                    description={carouselItems[0].description} />
                </a>
                {/* <FeatureDisplayUnit
                    image={carouselItems[0].src}
                    caption={carouselItems[0].caption}
                    description={carouselItems[0].description} /> */}

                <FeatureDisplayUnit
                    image={carouselItems[1].src}
                    caption={carouselItems[1].caption}
                    description={carouselItems[1].description} />

                <FeatureDisplayUnit
                    image={carouselItems[2].src}
                    caption={carouselItems[2].caption}
                    description={carouselItems[2].description} />

                <FeatureDisplayUnit
                    image={carouselItems[3].src}
                    caption={carouselItems[3].caption}
                    description={carouselItems[3].description} />

            </div>

        );
    }
}

class FeatureDisplayUnit extends Component {

    render() {

        return (
            <div className="feature-unit">
                <img className="feature-unit-img"
                    src={this.props.image} />
                <h3>{this.props.caption}</h3>
                <p>{this.props.description}</p>
            </div>
        )
    }
}

