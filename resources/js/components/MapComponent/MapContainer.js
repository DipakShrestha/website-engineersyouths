import React, { Component } from 'react';
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';

const mapStyles = {
    position: 'absolute',
    width: '100%',
    height: '100%'
};
export class MapContainer extends Component {
    constructor() {
        super();
        this.state = {
            showingInfoWindow: false,  //Hides or the shows the infoWindow
            activeMarker: {},          //Shows the active marker upon click
            selectedPlace: {}          //Shows the infoWindow to the selected place upon a marker
        };
        this.onMarkerClick = this.onMarkerClick.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    onMarkerClick(props, marker, e) {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });
    }
    onClose(props) {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            });
        }
    };

    render() {
        return (
            <Map
                google={this.props.google}
                zoom={19}
                // styles={mapStyles}
                // calssName="map"
                initialCenter={{
                    lat: 27.71962,
                    lng: 85.34486
                }}>
                <Marker
                    onClick={this.onMarkerClick}
                    name={'Engineers Youths'} />
                <InfoWindow
                    marker={this.state.activeMarker}
                    visible={this.state.showingInfoWindow}
                    onClose={this.onClose}>
                    <div >
                        <h6 >{this.state.selectedPlace.name}</h6>
                        <h8 >info@eyouths.com.np</h8>
                    </div>
                </InfoWindow>
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCGPnL2IGAK54ZZnhAsT1v01bDZdzBQQRM'
})(MapContainer);