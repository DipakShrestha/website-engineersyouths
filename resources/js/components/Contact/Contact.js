import React, { Component } from 'react';
import MapContainer from '../MapComponent/MapContainer';
import Header_Title from '../Header_All/header_all';
import '../../css/Contact.css';
import Form from '../Form/Form.js';


export default class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }


    render() {
        return (
            <div className="uk-container-expand" id="contact-view">
                <Header_Title title='Contact' />

                <div id="contact-top" style={{ paddingTop: "3vh" }}>
                    <div className="card uk-margin uk-flex uk-flex-center uk-flex-middle" id="contact-label" >
                        <img src={require("../../imageassest/iconReady.png")} alt="EY" className="uk-border-circle" style={{ "width": "70px", "height": "70px", "alignSelf": "center", "backgroundColor": " #e5e5e5" }} />
                        <h1 uk-scrollspy="cls: uk-animation-slide-left; delay: 500; repeat: true">Contact Us</h1>
                        <p uk-scrollspy="cls: uk-an          imation-slide-right; delay: 500; repeat: true">Engineers Youths</p>
                        <p className="title" uk-scrollspy="cls: uk-animation-slide-left; delay: 500; repeat: true">info@eyouths.com.np</p>
                        <p uk-scrollspy="cls: uk-animation-slide-right; delay: 500; repeat: true">Chabahil, Kathmandu</p>
                    </div>
                    <div className="card" id="form" >
                        <Form />
                    </div>
                </div>
                <div className="uk-container" >

                </div>
                <div className="uk-container-expand uk-flex uk-flex-center" style={{ position: 'relative', height: '400px', marginTop: '4vh', marginBottom: '2vh'}} >
                    <div style={{ width: '80%', height: '100%', position: 'absolute', borderStyle: 'solid',borderColor: 'gray' }} >
                        <MapContainer />
                    </div>
                </div>

            </div>

        );

    }
}



