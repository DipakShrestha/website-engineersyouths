import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

export default class BlogArticle extends Component {
    constructor(props){
        super(props);
        this.state={
            post: []
        };
    }

    componentWillMount(){
        axios.get("/api/test/"+this.props.match.params.id).then(response=>{
            this.setState({post: response.data[0]})
        }).catch(error => console.log(error));
    }
    
    render() {
        if(this.state.post){
            return (
                <div className="card">
                    <h1 className="card-header">{this.state.post.name}</h1>
                    <p className="card-body">{this.state.post.body}</p>
                    <p classname="card-body">{this.state.post.name}</p>
                </div>
                
            );
        }else{
            return null;
        }
        
    }
}



