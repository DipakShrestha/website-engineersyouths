import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class TestComponents extends Component {
    constructor(){
        super();
        this.state={
            blogs: []
        }
    }

    componentWillMount(){
        axios.get('/api/test').then(response=>{
            this.setState({
                blogs: response.data
            });
        }).catch(errors=>{
            console.log();
        })
    }
    render() {
        return (
            <div className="container">
                {this.state.blogs.map(blog=>
                    <li className="card-header">
                        <Link to={"/test/"+ blog.id}>{blog.name}</Link>
                    </li>
                )}
                {/* {this.state.blogs.map(blog =><li>{blog.body}</li>)} */}
            </div>
        );
    }
}

if (document.getElementById('testComponents')) {
    ReactDOM.render(<TestComponents />, document.getElementById('testComponents'));
}
