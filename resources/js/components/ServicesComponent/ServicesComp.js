import React, { Component } from 'react';
import Header_Title from '../Header_All/header_all';
import ReactDOM from "react-dom";

import '../../css/UIkit/uikit.css';
import '../../UIkitJS/uikit.js';
import '../../css/services-comp.css';


export default class ServicesComp extends Component {
    constructor(props) {
        super(props);
        this.handleReadMore = this.handleReadMore.bind(this);
        this.handleModalCalling = this.handleModalCalling.bind(this);
        this.state = {
            read_more_value: '',
            text: '',
            modal_title: '',
            image_list: [],
        }
    }

    handleReadMore(e) {
        // alert('value' + e);
        this.setState({
            read_more_value: e,
            // text:  e + 'hello',
        })
        if(e==="a"){
            this.setState({
                modal_title: 'Android & Ios Application',
                text: 'a hello',
                image_list: [
                    {img:require('../../imageassest/androidAndIosApplication.png'),key:'a1'},
                    {img:require('../../imageassest/androidAndIosApplication.png'),key:'a2'},
                    {img:require('../../imageassest/androidAndIosApplication.png'),key:'a3'},
                    {img:require('../../imageassest/androidAndIosApplication.png'),key:'a4'},
                ],
            })
        }else if(e==='b'){
            this.setState({
                modal_title: 'Website & Web Application',
                text: 'b hello',
                image_list: [
                    {img:require('../../imageassest/webapp_website.png'),key:'b1'},
                    {img:require('../../imageassest/webapp_website.png'),key:'b2'},
                    {img:require('../../imageassest/webapp_website.png'),key:'b3'},
                    {img:require('../../imageassest/webapp_website.png'),key:'b4'},
                ],
            })
        }else if(e==='c'){
            this.setState({
                modal_title: 'Embeded System',
                text: 'c hello',
                image_list: [
                    {img:require('../../imageassest/Embeded.png'),key:'c1'},
                    {img:require('../../imageassest/Embeded.png'),key:'c2'},
                    {img:require('../../imageassest/Embeded.png'),key:'c3'},
                    {img:require('../../imageassest/Embeded.png'),key:'c4'},
                ]
            })
        }else if(e==='d'){
            this.setState({
                modal_title: 'Led Matrix Display',
                text: 'd hello',
                image_list: [
                    {img:require('../../imageassest/led_display.jpeg'),key:'d1'},
                    {img:require('../../imageassest/led_display.jpeg'),key:'d2'},
                    {img:require('../../imageassest/led_display.jpeg'),key:'d3'},
                    {img:require('../../imageassest/led_display.jpeg'),key:'d4'},
                ]
            })
        }
    }

    handleModalCalling(){
        const value= this.state.read_more_value;
        // alert('value : '+ value);
        if(value==='a'){
            this.setState({
                text: 'a hello',
            })
            // return(
            //     <CustomModel text='Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

            //     Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                
            //     Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                
            //     Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                
            //     Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                
            //     Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                
            //     Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.' modal_title='Android & Ios Application'/>
            // )
        }else if(value==='b'){
            this.setState({
                text: 'b hello',
            })
            // return(
            //     <CustomModel text='Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

            //     Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                
            //     Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                
            //     Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                
            //     Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                
            //     Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                
            //     Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.' modal_title='Website & Web Application'/>
            // )
        }else if(value==='c'){
            return(
                <CustomModel text='Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                
                Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                
                Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                
                Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                
                Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                
                Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.' modal_title='Embeded System' />
            )
        }else if(value === 'd'){
            return(
                <CustomModel text='Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                
                Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                
                Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                
                Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                
                Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                
                Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.' modal_title='Led Matrix Display' />
            )
        }else{
            return(
                <CustomModel/>
            )
        }
    }
    
    
    render() {
        return (
            <div className="uk-container-extend" >
                <Header_Title title='Services' />
                <div id="services-top">
                    <div className="card" id="services-card">
                        <div className="uk-card-media-left uk-cover-container">
                            <img src={require("../../imageassest/androidAndIosApplication.png")} style={{objectFit: 'contain'}} alt="" uk-cover="true" />
                            <canvas width="600" height="400"></canvas>
                        </div>
                        <div className="uk-card-body">
                            <p style={{textAlign: 'center'}} >Remember us for any mobile apps- Android and ios.</p>
                        </div>
                        <div className="uk-card-footer">

                            <a className="uk-button uk-button-text" data-toggle="modal" href="#exampleModal" onClick={()=>this.handleReadMore('a')}>Read more</a>

                        </div>
                    </div>
                    <div className="card" id="services-card">
                        <div className="uk-card-media-left uk-cover-container">
                            <img src={require("../../imageassest/webapp_website.png")} alt="" uk-cover="true" />
                            <canvas width="600" height="400"></canvas>
                        </div>
                        <div className="uk-card-body">
                            <p style={{textAlign: 'center'}}>Remember us for all kinds of Website and Webapp development.</p>
                        </div>
                        <div className="uk-card-footer">
                            <a className="uk-button uk-button-text"   data-toggle="modal" href="#exampleModal" onClick={()=>this.handleReadMore('b')}>Read more</a>
                        </div>
                    </div>
                    <div className="card" id="services-card">
                        <div className="uk-card-media-left uk-cover-container">
                            <img src={require("../../imageassest/Embeded.png")} alt="" uk-cover="true" />
                            <canvas width="600" height="400"></canvas>
                        </div>
                        <div className="uk-card-body">
                            <p style={{textAlign: 'center'}}>We develop power efficient and optimized embedded systems.</p>
                        </div>
                        <div className="uk-card-footer">
                            <a className="uk-button uk-button-text"   data-toggle="modal" href="#exampleModal" onClick={()=>this.handleReadMore('c')}>Read more</a>
                        </div>
                    </div>
                    <div className="card" id="services-card">
                        <div className="uk-card-media-left uk-cover-container">
                            <img className="uk-border-rounded" src={require("../../imageassest/led_display.jpeg")} alt="Border rounded" uk-cover="true" />
                            <canvas width="600" height="400"></canvas>
                        </div>
                        <div className="uk-card-body">
                            <p style={{textAlign: 'center'}}>We develop display board as per your need of all sizes and with any customization.</p>
                        </div>
                        <div className="uk-card-footer">
                            <a className="uk-button uk-button-text"   data-toggle="modal" href="#exampleModal" onClick={()=>this.handleReadMore('d')}>Read more</a>
                        </div>
                    </div>

                </div>
                {/* <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Launch demo modal
                </button> */}
                <CustomModel text = {this.state.text} modal_title={this.state.modal_title} image_list={this.state.image_list}/>
                {/* <this.handleModalCalling/> */}
            </div>
        );
    }
}

class CustomModel extends Component {
    
    render() {
        const image_list = this.props.image_list;
        var flag = true;
        const renderCarouselImage=image_list.map(image=>{
            if(flag){
                flag = false;
                
                return(
                    <div key={image.key} className="carousel-item active">
                        <img className="d-block w-100" src={image.img} alt="First slide"/>
                    </div>
                );              
            }
            return(
                <div key={image.key} className="carousel-item">
                    <img className="d-block w-100" src={image.img} alt="Second slide"/>
                </div>
            )
        })
        // const textBody = this.state.text_body;
        return (
            <div className="modal fade bd-example-modal-lg" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg"  role="document">
                    
                    <div className="modal-content">
                        {/* <button className="uk-modal-close-default" type="button" uk-close></button> */}   

                        <div className="modal-header">
                            <h2 className="modal-title" id="exampleModalLabel" >{this.props.modal_title}</h2>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body " >
                            <div className="uk-panel uk-panel-scrollable uk-height-large" style={{fontSize: '1.5vw', textAlign:'justify'}} >
                                {this.props.text}
                                
                            </div>
                        </div>
                        
                    </div>
                    <div className="modal-content">
                        {/* <button className="uk-modal-close-default" type="button" uk-close></button> */}  
                        <div className="modal-body">
                            <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                                <div className="carousel-inner">
                                    
                                    {renderCarouselImage}
                                    
                                </div>
                                <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Previous</span>
                                </a>
                                <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Next</span>
                                </a>
                            </div>
                            {/* <div class="uk-panel uk-panel-scrollable" style={{fontSize: '1.3vw', textAlign:'justify', height: '400px',display: 'flex', flexDirection: 'row',position: 'relative'}} >
                                {this.props.text}
                                <CarouselView/>
                                
                                <img src={require("../../imageassest/Embeded.png")} alt=""/>
                                <img src={require("../../imageassest/Embeded.png")} alt=""/>
                            </div> */}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            {/* <button type="button" className="btn btn-primary">Save changes</button> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}