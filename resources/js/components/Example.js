import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Header_Title from './Header_All/header_all';
import { CarouselView } from '../index';
// import Home from './Home';


export default class Example extends Component {
    render() {
        return (
            <div className="uk-container-extend" style={{background:'white'}}>
                {/* heading division */}
                <Header_Title title='Home' />
                {/* <div className="">

                    <div className=" uk inline blur-test-background" uk-scrollspy="cls: uk-animation-slide-left; delay:100; repeat: true">

                        <div class="content"  >
                            <div class="text">

                                <h4 className="d-flex justify-content-center " id="text1" uk-scrollspy="cls: uk-animation-slide-right; delay:500; repeat: true">Welcome to</h4>
                                <h1 className="d-flex justify-content-center color-main-text " id="h2" uk-scrollspy="cls: uk-animation-slide-left; delay:500; repeat: true">Engineers Youths</h1>
                                <h4 className="d-flex justify-content-center color-main-below-text" id="h4" uk-scrollspy="cls: uk-animation-slide-right; delay:500; repeat: true">We are dedicated toward the perfection</h4>
                            </div>
                            <div class="blur"></div>
                        </div>
                    </div>
                </div> */}
                <div className="uk-flex uk-flex-center" style={{paddingTop: '3%'}}>

                    <div className="uk-container">

                        <CarouselView></CarouselView>

                    </div>
                </div>
                <div className="uk-flex uk-flex-center" uk-scrollspy="cls: uk-animation-slide-right; repeat: true">
                    <div>
                        <h1><span className="uk-label uk-label-warning">We Aim To...</span></h1>
                        {/* <button class="uk-button uk-button-default" uk-tooltip="Hello World">Hover</button> */}

                    </div>
                </div>
                <div className="uk-flex uk-text-justify" uk-scrollspy="cls: uk-animation-slide-right; repeat: true">
                    <h2 className="uk-dropcap">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h2>
                </div>

            </div>

            // <div className="container">
            //     <div className="row justify-content-center">
            //         <div className="col-md-8">
            //             <div className="card">
            //                 <div className="card-header">my First web using laravel and react</div>

            //                 <div className="card-body">
            //                     hello i am dipak shrestha
            //                     <div>
            //                         hello i am apple
            //                     </div>
            //                     <div className="card-header">
            //                         hello i am ball
            //                     </div>
            //                 </div>
            //                 <div className="container">
            //                     hello second div
            //                     <div>
            //                         hello i am cat
            //                     </div>
            //                     <div>
            //                         hello i am dog
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>
            // </div>

        );
    }
}

// if (document.getElementById('example')) {
//     ReactDOM.render(<Example />, document.getElementById('example'));
// }
