import React, { Component } from 'react';
import ReactPhoneInput from 'react-phone-input-2';

class Form extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAdressChange = this.handleAdressChange.bind(this);
        this.handleContactChange = this.handleContactChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleConfirmEmailChange = this.handleConfirmEmailChange.bind(this);
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleEmailInputError = this.handleEmailInputError.bind(this);
        this.handleConfirmEmailInputError = this.handleConfirmEmailInputError.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            name: '',
            address: '',
            contact: '',
            email: '',
            confirm_email: '',
            c_message: '',
            email_match_confirm_email: true,
            check_length_contact: false,
        }
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSubmit(e) {
        e.preventDefault();
        const { name, address, contact, email, confirm_email, c_message, check_length_contact } = this.state;
        // const form = await axios.post('/api/form',{
        //     name,    
        //     email,
        //     message 
        // }) 
        // alert('length of contact : ' + this.state.contact.toString().length);
        if (this.state.contact.toString().length == 14) {

            this.setState({
                check_length_contact: true,
            })
        } else {
            this.setState({
                check_length_contact: false,
            })
        }

        if (!(this.state.email.toLowerCase() === this.state.confirm_email.toLowerCase())) {
            this.setState({
                email_match_confirm_email: false,
            })
            return;
        } else {
            this.setState({
                email_match_confirm_email: true,
            })
            axios.post('/p', this.state).then(response => {
                console.log(response);
                if (response.status == 200) {
                    alert('Success! Your detail has been sent. Be patient, we will reply you within 24 hours. Thank You!');
                }
            }).then(error => {
                console.log(error);
                // alert('Failed!! due to ' + error);
            });
        }
    }

    handleNameChange(event) {
        this.setState({
            name: event.target.value,
        })
    }

    handleAdressChange(e) {
        this.setState({
            address: e.target.value
        })
    }

    handleContactChange(e) {
        this.setState({
            contact: e
        })
    }

    handleEmailChange(e) {
        this.setState({
            email: e.target.value,
        })
    }

    handleMessageChange(e) {
        this.setState({
            c_message: e.target.value,
        })
    }

    handleConfirmEmailChange(e) {
        this.setState({
            confirm_email: e.target.value,
        })
    }

    handleEmailInputError(props) {
        if (this.state.email_match_confirm_email) {
            return (
                <div className="form-group"  >
                    <small className="form-text" style={{ "color": "#00ff00" }}>*</small>
                    <input className="uk-input form-control" uk-tooltip="title:Enter your Email Address; pos:top-left; offset:1" style={{ "borderRadius": "5px" }} type="email" placeholder="Enter Email: foo@example.com" name="email" onChange={this.handleEmailChange} required />
                </div>
            )
        } else {
            return (
                <div className="form-group"  >
                    <small className="form-text" style={{ "color": "#ff0000" }}>* your below email must match with this email</small>
                    <input className="uk-input form-control uk-form-danger" uk-tooltip="title:Enter your Email Address; pos:top-left; offset:1" style={{ "borderRadius": "5px" }} type="email" placeholder="Enter Email: foo@example.com" name="email" onChange={this.handleEmailChange} required />
                </div>
            )
        }
    }
    handleConfirmEmailInputError(props) {
        if (this.state.email_match_confirm_email) {
            return (
                <div className="form-group" >
                    {/* <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend2" style={{ "color": "#ff0000", "background": "#fff", "borderColor": "#fff" }}>*</span>
                            </div> */}
                    < small className="form-text" style={{ "color": "#00ff00" }}>*</small >
                    <input className="uk-input form-control" uk-tooltip="title:Confirm your Email Address; pos:top-left; offset:1" style={{ "borderRadius": "5px" }} type="email" placeholder="Confirm Email: foo@example.com" name="confirm_email" onChange={this.handleConfirmEmailChange} required />

                </div >
            )
        } else {
            return (
                < div className="form-group" >
                    <small className="form-text" style={{ "color": "#ff0000" }}>* this email must match with above email</small >
                    <input className="uk-input form-control uk-form-danger" uk-tooltip="title:Confirm your Email Address; pos:top-left; offset:1" style={{ "borderRadius": "5px" }} type="email" placeholder="Confirm Email: foo@example.com" name="confirm_email" onChange={this.handleConfirmEmailChange} required />

                </div >
            )
        }
    }

    render() {
        return (
            <form className="needs-validation" onSubmit={this.handleSubmit} style={{ margin: '0px', "justifyContent": "space-evenly", "position": "relative" }}>
                <fieldset className="uk-fieldset">
                    <legend className="uk-legend" uk-scrollspy="cls: uk-animation-slide-top; delay:1000; repeat: true" style={{ "textAlign": "center", "marginTop": "10px" }}>Query Form</legend>

                    <div className="form-group" >
                        <small className="form-text" style={{ "color": "#00ff00" }}>*</small>
                        <input className="uk-input form-control" type="" uk-tooltip="title:Enter your Full Name; pos: top-left; offset:1" style={{ "borderRadius": "5px" }} placeholder="Full Name" name="name" onChange={this.handleNameChange} validate={this.validate} required />
                    </div>

                    <div className=" form-group" >

                        <input className="uk-input" uk-tooltip="title: Enter your Address; pos:top-left; offset:1" type="text" style={{ "borderRadius": "5px" }} placeholder="Address" name='address' onChange={this.handleAdressChange} />
                    </div>

                    <div className="form-group" uk-tooltip="title: Enter your Phone Number; pos: top-left; offset:1">

                        <ReactPhoneInput
                            value={this.state.contact} defaultCountry={'np'} name="contact" onChange={this.handleContactChange} />

                        {/* <input className="uk-input" type="phone-number" placeholder="Contact No"/> */}
                    </div>

                    <this.handleEmailInputError />
                    <this.handleConfirmEmailInputError />

                    <div className="form-group" >
                        <small className="form-text" style={{ "color": "#00ff00" }}>*</small>
                        <textarea className="uk-textarea form-control" required uk-tooltip="title: Enter Message; pos: top-left; offset:1; cls:uk-active" style={{ "borderRadius": "5px" }} rows="3" placeholder="Enter your message" ref={(input) => this.input = input} name="c_message" onChange={this.handleMessageChange} ></textarea>
                        <small className="form-text" style={{ "color": "#000000", "textAlign": "center" }}>Note:<span style={{ "color": "#00ff00" }}>* </span><span style={{ "color": "#000000" }}>indicates fields are required</span></small>
                    </div>
                    <div className="form-group"  style={{ "display": "flex", "justifyContent": "center", "position": "relative" }}>
                        <button className="uk-button uk-button-default" style={{ "borderRadius": "5px" }} type="submit">Send</button>

                    </div>
                </fieldset>

            </form>
        );
    }
}
export default Form;

