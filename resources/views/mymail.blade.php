<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Name: {{$name}}</h1>
    <br/>
    <h3>Address: {{$address}} </h3>
    <h3>Contact No: {{$contact}}</h3>
    <h3>Email: {{$confirm_email}}</h3>
    <br/>
    <h2>Message from Customer:</h2>
    <br/>
    <h5>{{$c_message}}</h5>

</body>
</html>