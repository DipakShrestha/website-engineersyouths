const express = require('express')
const bodyParser = require('body-parser')
const nodeMailer = require('nodemailer')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

// app.post('/api/form', (req, res)=>{
//     console.log(req.body)
// })

const PORT = process.env.PORT || 8000

app.listen(PORT,()=> {
    console.log(`Server listening on port ${PORT}`)
})