<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request ;

class MyMail extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        return $this->view('mymail',['name'=>$request->name,'address'=>$request->address,'contact'=>$request->contact,'confirm_email'=>$request->confirm_email,'c_message'=>$request->c_message])->to('info@eyouths.com.np')->subject('eYouths_Query');
    }
}
